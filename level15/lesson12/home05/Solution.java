package com.javarush.test.level15.lesson12.home05;

/* Перегрузка конструкторов
1. В классе Solution создайте по 3 конструктора для каждого модификатора доступа.
2. В отдельном файле унаследуйте класс SubSolution от класса Solution.
3. Внутри класса SubSolution создайте конструкторы командой Alt+Insert -> Constructors.
4. Исправьте модификаторы доступа конструкторов в SubSolution так, чтобы они соответствовали конструкторам класса Solution.
*/

import java.math.BigDecimal;

public class Solution {

    private Solution(Object value){}
    private Solution(Integer value){}
    private Solution(String value){}

    protected Solution(Character value){}
    protected Solution(Double value){}
    protected Solution(Float value){}

    public Solution(){}
    public Solution(Number value){}
    public Solution(Boolean value){}

    Solution(BigDecimal value){}
    Solution(Byte value){}
    Solution(Short value){}
}

