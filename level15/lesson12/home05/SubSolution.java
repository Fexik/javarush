package com.javarush.test.level15.lesson12.home05;

import java.math.BigDecimal;

/**
 * Created by Fexik on 06.10.2015.
 */
public class SubSolution extends Solution
{
    private SubSolution(Object value){
        super();
    }
    private SubSolution(Integer value){
        super();
    }
    private SubSolution(String value){
        super();
    }

    protected SubSolution(Character value)
    {
        super(value);
    }

    protected SubSolution(Double value)
    {
        super(value);
    }

    protected SubSolution(Float value)
    {
        super(value);
    }

    public SubSolution(Number value)
    {
        super(value);
    }

    public SubSolution(Exception value)
    {
        super();
    }

    public SubSolution(Boolean value)
    {
        super(value);
    }

    SubSolution(Short value)
    {
        super(value);
    }

    SubSolution(BigDecimal value)
    {
        super(value);
    }

    SubSolution(Byte value)
    {
        super(value);
    }
}
