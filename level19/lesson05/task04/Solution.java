package com.javarush.test.level19.lesson05.task04;

/* Замена знаков
Считать с консоли 2 имени файла.
Первый Файл содержит текст.
Заменить все точки "." на знак "!", вывести во второй файл.
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String nameInput = scanner.next();
        String nameOutput = scanner.next();
        scanner.close();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(
                                new File(nameInput)),"UTF-8"));
        FileWriter out = new FileWriter(nameOutput,false);
        StringBuilder str = new StringBuilder();
        while (in.ready()){
            out.write(in.readLine().replaceAll("\\.","\\!"));
        }
        in.close();
        out.close();
    }
}
