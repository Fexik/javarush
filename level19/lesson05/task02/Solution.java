package com.javarush.test.level19.lesson05.task02;

/* Считаем слово
Считать с консоли имя файла.
Файл содержит слова, разделенные знаками препинания.
Вывести в консоль количество слов "world", которые встречаются в файле.
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {

    public static void main(String[] args) throws IOException
    {
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(
                                new File(rd.readLine())),"UTF-8"));
        int count = 0;
        while (in.ready())
        {
            String str = in.readLine();
            Pattern p = Pattern.compile("\\b[Ww]orld\\b",Pattern.UNICODE_CASE);
            Matcher m = p.matcher(str);

            while (m.find()){
                count++;
            }
        }
        System.out.println(count);
        rd.close();
        in.close();
    }
}
