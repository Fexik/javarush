package com.javarush.test.level19.lesson05.task03;

/* Выделяем числа
Считать с консоли 2 имени файла.
Вывести во второй файл все числа, которые есть в первом файле.
Числа выводить через пробел.
Закрыть потоки. Не использовать try-with-resources

Пример тела файла:
12 text var2 14 8v 1

Результат:
12 14 1
*/

import java.io.*;
import java.util.Scanner;
import java.util.regex.*;

public class Solution {
    public static void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);
        String nameInput = scanner.next();
        String nameOutput = scanner.next();
        scanner.close();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(
                                new File(nameInput)),"UTF-8"));
        FileWriter out = new FileWriter(nameOutput);
        StringBuilder str = new StringBuilder();
        while (in.ready()){

            Pattern regex = Pattern.compile("\\b\\d+\\b");
            Matcher regexMatcher = regex.matcher(in.readLine());

            while (regexMatcher.find()){
                str.append(regexMatcher.group() + " ");
            }
        }
        out.write(str.toString());
        in.close();
        out.close();


    }
}
