package com.javarush.test.level19.lesson05.task05;

/* Пунктуация
Считать с консоли 2 имени файла.
Первый Файл содержит текст.
Удалить все знаки пунктуации, вывести во второй файл.
http://ru.wikipedia.org/wiki/%D0%9F%D1%83%D0%BD%D0%BA%D1%82%D1%83%D0%B0%D1%86%D0%B8%D1%8F
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String nameInput = scanner.next();
        String nameOutput = scanner.next();
        scanner.close();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(
                                new File(nameInput)),"UTF-8"));

        FileWriter out = new FileWriter(nameOutput,false);

        while (in.ready()){
            out.write(in.readLine().replaceAll("\\p{P}",""));
        }
        in.close();
        out.close();
    }
}
