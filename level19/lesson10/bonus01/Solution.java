package com.javarush.test.level19.lesson10.bonus01;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/* Отслеживаем изменения
Считать в консоли 2 имени файла - file1, file2.
Файлы содержат строки, file2 является обновленной версией file1, часть строк совпадают.
Нужно создать объединенную версию строк, записать их в список lines
Операции ADDED и REMOVED не могут идти подряд, они всегда разделены SAME
Пример:
оригинальный   редактированный    общий
file1:         file2:             результат:(lines)

строка1        строка1            SAME строка1
строка2                           REMOVED строка2
строка3        строка3            SAME строка3
строка4                           REMOVED строка4
строка5        строка5            SAME строка5
               строка0            ADDED строка0
строка1        строка1            SAME строка1
строка2                           REMOVED строка2
строка3        строка3            SAME строка3
               строка5            ADDED строка5
строка4        строка4            SAME строка4
строка5                           REMOVED строка5

c:/JavaRush/data.txt
c:/JavaRush/data2.txt

*/

public class Solution {
    public static List<LineItem> lines = new ArrayList<LineItem>();

    public static void main(String[] args) throws IOException{
        Scanner scanner = new Scanner(System.in);
        String file1 = scanner.nextLine();
        String file2 = scanner.nextLine();
        scanner.close();

        Scanner f1 = new Scanner(new File(file1));
        Scanner f2 = new Scanner(new File(file2));

        List<String> list1 = new ArrayList<>();
        List<String> list2 = new ArrayList<>();

        while ( f1.hasNextLine()){
            list1.add(f1.nextLine());
        }

        while ( f2.hasNextLine()){
            list2.add(f2.nextLine());
        }

        Type t = null;
        String str = null;

        for (int i = 0; i < list1.size(); i++)
        {
            str = list1.get(i);
            try
            {
                if (list1.get(i).equals(list2.get(i))) {
                    t = Type.SAME;
                } else if (list2.get(i).equals("")) {
                    t = Type.REMOVED;
                } else if (list1.get(i).equals("")) {
                    t = Type.ADDED;
                    str = list2.get(i);
                }
                lines.add(new LineItem(t, str));
            }
            catch (IndexOutOfBoundsException e)
            {
                lines.add(new LineItem(Type.REMOVED, str));
            }

        }

        f1.close();
        f2.close();

        for (LineItem l : lines){
            System.out.println(l.type + " " + l.line);
        }
    }


    public static enum Type {
        ADDED,        //добавлена новая строка
        REMOVED,      //удалена строка
        SAME          //без изменений
    }

    public static class LineItem {
        public Type type;
        public String line;

        public LineItem(Type type, String line) {
            this.type = type;
            this.line = line;
        }
    }
}
