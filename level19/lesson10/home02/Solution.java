package com.javarush.test.level19.lesson10.home02;

/* Самый богатый
В метод main первым параметром приходит имя файла.
В этом файле каждая строка имеет следующий вид:
имя значение
где [имя] - String, [значение] - double. [имя] и [значение] разделены пробелом

Для каждого имени посчитать сумму всех его значений
Вывести в консоль имена, у которых максимальная сумма
Имена разделять пробелом либо выводить с новой строки
Закрыть потоки. Не использовать try-with-resources

Пример входного файла:
Петров 0.501
Иванов 1.35
Петров 0.85

Пример вывода:
Петров
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) throws IOException {
        Map<String, Double> map = new TreeMap<>();

        String fileName = args[0];
        //String fileName = "c:/JavaRush/data.txt";

        BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(fileName),"UTF-8"));

        String[] str;
        double value = 0;
        while (in.ready()){
            str = in.readLine().split(" ");
            value = Double.parseDouble(str[1]);

            if (map.containsKey(str[0])) {
                value += map.get(str[0]);
            }
            map.put(str[0], value);

        }
        double max = 0;
        String name = null;
        for (Map.Entry<String, Double> pair : map.entrySet()){
            if (max < pair.getValue()){
                max = pair.getValue();
                name = pair.getKey();
            }
        }
        System.out.println(name);
        in.close();
    }
}
