package com.javarush.test.level19.lesson10.home07;

/* Длинные слова
В метод main первым параметром приходит имя файла1, вторым - файла2
Файл1 содержит слова, разделенные пробелом.
Записать через запятую в Файл2 слова, длина которых строго больше 6
Закрыть потоки. Не использовать try-with-resources

Пример выходных данных:
длинное,короткое,аббревиатура
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Solution {
    public static void main(String[] args) throws IOException {
        String fileName1 = args[0];
        String fileName2 = args[1];
        //String fileName1 = "c:/JavaRush/data.txt";
        //String fileName2 = "c:/JavaRush/data2.txt";

        BufferedReader in = new BufferedReader(new FileReader(fileName1));
        FileWriter out = new FileWriter(fileName2);

        StringBuilder text = new StringBuilder();
        while (in.ready())
        {
            String[] words = in.readLine().split(" ");
            for (String s : words){

                if (s.length() > 6 )
                    text.append(s + ",");
            }
        }
        String str = text.toString();
        out.write(str.substring(0, str.length()-1));
        in.close();
        out.close();

    }
}
