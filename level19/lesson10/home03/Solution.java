package com.javarush.test.level19.lesson10.home03;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/* Хуан Хуанович
В метод main первым параметром приходит имя файла.
В этом файле каждая строка имеет следующий вид:
имя день месяц год
где [имя] - может состоять из нескольких слов, разделенных пробелами, и имеет тип String
[день] - int, [месяц] - int, [год] - int
данные разделены пробелами

Заполнить список PEOPLE импользуя данные из файла
Закрыть потоки. Не использовать try-with-resources

Пример входного файла:
Иванов Иван Иванович 31 12 1987
Вася 15 5 2013
*/

public class Solution
{
    public static final List<Person> PEOPLE = new ArrayList<Person>();

    public static void main(String[] args) throws IOException
    {

        String fileName = args[0];
        //String fileName = "c:/JavaRush/data.txt";

        BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF-8"));

        String[] str;
        StringBuilder fullName = new StringBuilder();
        while (in.ready())
        {
            str = in.readLine().split(" ");

            int count = str.length;
            Date date = new GregorianCalendar(Integer.parseInt(str[count - 1]), Integer.parseInt(str[count - 2]) - 1, Integer.parseInt(str[count - 3])).getTime();
            for (int i = 0; i < count - 3; i++)
            {
                fullName.append(str[i]+" ");
            }
            PEOPLE.add(new Person(fullName.toString().trim(), date));
            fullName.setLength(0);

        }

        in.close();
    }
}

