package com.javarush.test.level19.lesson10.bonus03;

/* Знакомство с тегами
Считайте с консоли имя файла, который имеет HTML-формат
Пример:
Info about Leela <span xml:lang="en" lang="en"><b><span>Turanga Leela
</span></b></span><span>Super</span><span>girl</span>
Первым параметром в метод main приходит тег. Например, "span"
Вывести на консоль все теги, которые соответствуют заданному тегу
Каждый тег на новой строке, порядок должен соответствовать порядку следования в файле
Количество пробелов, \n, \r не влияют на результат
Файл не содержит тег CDATA, для всех открывающих тегов имеется отдельный закрывающий тег, одиночных тегов нету
Тег может содержать вложенные теги
Пример вывода:
<span xml:lang="en" lang="en"><b><span>Turanga Leela</span></b></span>
<span>Turanga Leela</span>
<span>Super</span>
<span>girl</span>

Шаблон тега:
<tag>text1</tag>
<tag text2>text1</tag>
<tag
text2>text1</tag>

text1, text2 могут быть пустыми

c:/JavaRush/data.html
*/

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Solution {
    public static void main(String[] args) throws IOException{

        if (args.length == 0) return;

        String tag = args[0];
        Scanner scanner = new Scanner(System.in);
        String file1 = scanner.nextLine();
        scanner.close();

        Scanner in = new Scanner(new FileReader(file1));
        StringBuilder str = new StringBuilder();
        List<String> lines = new ArrayList<String>();

        while (in.hasNext()) str.append(in.nextLine());

        in.close();

        Pattern p1 = Pattern.compile("<("+tag+")(.+?)></\\1>");
        Pattern p2 = Pattern.compile("<("+tag+")>.*?</\\1>");
        Matcher m1 = p1.matcher(str);
        Matcher m2 = p2.matcher(str);

        while (m1.find()) lines.add(m1.group());
        while (m2.find()) lines.add(m2.group());

        for (String l : lines) System.out.println(l);

        /*Решение не правильное*/
    }
}
