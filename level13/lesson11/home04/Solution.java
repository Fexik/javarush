package com.javarush.test.level13.lesson11.home04;

/* Запись в файл
1. Прочесть с консоли имя файла.
2. Считывать строки с консоли, пока пользователь не введет строку "exit".
3. Вывести абсолютно все введенные строки в файл, каждую строчку с новой стороки.
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args)
    {
        try
        {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String path = br.readLine();
            //InputStream fs = new FileInputStream(nameFile);

            FileWriter f = new FileWriter(path);
            //f.write(path+"\n");
            while(true){
                String str = br.readLine();
                f.write(str+"\n");
                if (str.equals("exit")) {
                    f.close();
                    break;
                }
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

    }
}
