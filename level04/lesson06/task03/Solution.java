package com.javarush.test.level04.lesson06.task03;

/* Сортировка трех чисел
Ввести с клавиатуры три числа, и вывести их в порядке убывания. Без использования If.
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int a = Integer.parseInt(reader.readLine());
        int b = Integer.parseInt(reader.readLine());
        int c = Integer.parseInt(reader.readLine());
        int sum = a + b + c;
        int max = max(a, max(b, c));
        int min = min(a, min(b, c));

        System.out.println(max);
        System.out.println(sum - max - min );
        System.out.println(min);

    }

    public  static int max(int n1, int n2){
        return  (n1 > n2) ? n1 : n2;
    }

    public  static int min(int n1, int n2){
        return  (n1 < n2) ? n1 : n2;
    }
}
