package com.javarush.test.level04.lesson06.task01;

/* Минимум двух чисел
Ввести с клавиатуры два числа, и вывести на экран минимальное из них.
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String a = reader.readLine();
        String b = reader.readLine();
        System.out.println(min(Integer.parseInt(a), Integer.parseInt(b)));

    }

    private static int min(int a, int b){
        int min = a;
        if (min > b)
            min = b;
        return min;
    }
}