package com.javarush.test.level04.lesson16.home02;

import java.io.*;

/* Среднее такое среднее
Ввести с клавиатуры три числа, вывести на экран среднее из них. Т.е. не самое большое и не самое маленькое.
*/

public class Solution
{
    public static void main(String[] args)   throws Exception
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int a = Integer.parseInt(br.readLine());
        int b = Integer.parseInt(br.readLine());
        int c = Integer.parseInt(br.readLine());
        int sum = a + b + c;
        int mid = sum - (max(a, max(b, c)))-(min(a, min(b, c)));
        System.out.println(mid);
    }

    public static int min(int n, int m){
        return (m < n)? m : n;
    }

    public static int max(int n, int m){
        return (m > n)? m : n;
    }
}
