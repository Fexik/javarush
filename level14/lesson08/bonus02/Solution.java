package com.javarush.test.level14.lesson08.bonus02;

/* НОД
Наибольший общий делитель (НОД).
Ввести с клавиатуры 2 целых положительных числа.
Вывести в консоль наибольший общий делитель.
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int n1 = Integer.parseInt(br.readLine());
        int n2 = Integer.parseInt(br.readLine());
        br.close();

        nod(n1, n2);
        nod2(n1, n2);
        System.out.println(nod3(n1, n2));


    }

    //перший варіант
    static void nod(int a, int b){
        if (a > b) a = a - b; else b = b - a;
        if (b == 0)
        {
            System.out.println(a);
            return;
        }
        System.out.println(nod3(a, b));
    }

    //другий варіант
    static void nod2(int a, int b){
        if (b == 0) {
            System.out.println(a);
            return;
        }
        else nod(b, a%b);
    }

    //третій варіант
    static int nod3(int a, int b){
        if (b==0) return a;
        return nod3(b, a%b);
    }
}
