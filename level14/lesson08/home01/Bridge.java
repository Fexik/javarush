package com.javarush.test.level14.lesson08.home01;

/**
 * Created by Fexik on 24.09.2015.
 */
public interface Bridge
{
    int getCarsCount();
}
