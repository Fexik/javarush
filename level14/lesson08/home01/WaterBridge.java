package com.javarush.test.level14.lesson08.home01;

/**
 * Created by Fexik on 24.09.2015.
 */
public class WaterBridge implements Bridge
{
    @Override
    public int getCarsCount()
    {
        return 20;
    }
}
