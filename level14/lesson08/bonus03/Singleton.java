package com.javarush.test.level14.lesson08.bonus03;

/**
 * Created by Fexik on 29.09.2015.
 */

public class Singleton{
    private Singleton(){}

    private static class SingletonHelper{
        private static final Singleton INSTANCE = new Singleton();
    }

    public static Singleton getInstance(){
        return SingletonHelper.INSTANCE;
    }
}

/*public class Singleton
{

    private static Singleton instance;

    private Singleton()
    {

    }

    public static Singleton getInstance(){
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }
}*/
