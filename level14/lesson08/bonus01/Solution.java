package com.javarush.test.level14.lesson08.bonus01;

import java.util.*;

/* Нашествие эксепшенов
Заполни массив exceptions 10 различными эксепшенами.
Первое исключение уже реализовано в методе initExceptions.
*/

public class Solution
{
    public static List<Exception> exceptions = new ArrayList<Exception>();

    public static void main(String[] args)
    {
        initExceptions();

        for (Exception exception : exceptions)
        {
            System.out.println(exception);
        }
    }

    private static void initExceptions()
    {   //it's first exception
        try
        {
            float i = 1 / 0;

        } catch (Exception e)
        {
            exceptions.add(e);
        }

        //Add your code here
        try
        {
            int a[] = { 1 };
            a[4] = 99;

        } catch (Exception e){
            exceptions.add(e);
        }

        try
        {
            throw new IllegalAccessException();

        } catch (Exception e){
            exceptions.add(e);
        }

        try
        {
            throw new ArrayStoreException();

        } catch (Exception e){
            exceptions.add(e);
        }

        try
        {
            throw new ClassCastException();
        } catch (Exception e){
            exceptions.add(e);
        }

        try
        {
            throw new ConcurrentModificationException();

        } catch (Exception e){
            exceptions.add(e);
        }

        try
        {
            throw new EmptyStackException();
        } catch (Exception e){
            exceptions.add(e);
        }

        try
        {
            throw new IllegalMonitorStateException();
        } catch (Exception e){
            exceptions.add(e);
        }

        try
        {
            throw new StringIndexOutOfBoundsException();
        } catch (Exception e){
            exceptions.add(e);
        }

        try
        {
            throw new NumberFormatException();
        } catch (Exception e){
            exceptions.add(e);
        }


    }
}
