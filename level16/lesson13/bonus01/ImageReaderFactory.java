package com.javarush.test.level16.lesson13.bonus01;

import com.javarush.test.level16.lesson13.bonus01.common.*;

/**
 * Created by Fexik on 26.10.2015.
 */
public class ImageReaderFactory
{
    public static ImageReader getReader(ImageTypes imageTypes)
    {
        ImageReader ir;

        if (imageTypes == ImageTypes.JPG)
        {
            ir = new JpgReader();
        }
        else if (imageTypes == ImageTypes.BMP)
        {
            ir = new BmpReader();
        }
        else if (imageTypes == ImageTypes.PNG)
        {
            ir = new PngReader();
        }
        else
        {
            throw  new  IllegalArgumentException("Неизвестный тип картинки");
        }
        return ir;
    }
}
