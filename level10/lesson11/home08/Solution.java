package com.javarush.test.level10.lesson11.home08;


import java.util.ArrayList;

/* Массив списков строк
Создать массив, элементами которого будут списки строк. Заполнить массив любыми данными и вывести их на экран.
*/

public class Solution
{
    public static void main(String[] args)
    {
        ArrayList<String>[] arrayOfStringList =  createList();
        printList(arrayOfStringList);
    }

    public static ArrayList<String>[] createList()
    {
        ArrayList<String>[] list = new ArrayList[5];
        ArrayList<String> data = new ArrayList<String>();

        data.add("Мама");
        data.add(" ");
        data.add("Мыла");
        data.add(" ");
        data.add("Раму");
        //------------
        list[0] = data;
        list[1] = data;
        list[2] = data;
        list[3] = data;
        list[4] = data;

        return list;
    }

    public static void printList(ArrayList<String>[] arrayOfStringList)
    {
        for (ArrayList<String> list: arrayOfStringList)
        {
            for (String s : list)
            {
                System.out.println(s);
            }
        }
    }
}