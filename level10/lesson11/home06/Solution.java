package com.javarush.test.level10.lesson11.home06;

/* Конструкторы класса Human
Напиши класс Human с 6 полями. Придумай и реализуй 10 различных конструкторов для него. Каждый конструктор должен иметь смысл.
*/

public class Solution
{
    public static void main(String[] args)
    {

    }

    public static class Human
    {
        public String name;
        public int age;
        public boolean sex;
        public int height;
        public int weight;
        public String work;

        public Human(String name){
            this.name = name;
        }
        public Human(String name, int age){
            this.name = name;
            this.age = age;
        }
        public Human(String name, int age, boolean sex){
            this.name = name;
            this.age = age;
            this.sex = sex;
        }

        public Human(String name, int age, boolean sex, int height)
        {
            this.name = name;
            this.age = age;
            this.sex = sex;
            this.height = height;
        }

        public Human(String name, int age, boolean sex, int height, int weight)
        {
            this.name = name;
            this.age = age;
            this.sex = sex;
            this.height = height;
            this.weight = weight;
        }

        public Human(String name, int age, boolean sex, int height, int weight, String work)
        {
            this.name = name;
            this.age = age;
            this.sex = sex;
            this.height = height;
            this.weight = weight;
            this.work = work;
        }

        public Human(String name, int age, boolean sex, String work)
        {
            this.name = name;
            this.age = age;
            this.sex = sex;
            this.work = work;
        }

        public Human(String name, String work, int age)
        {
            this.name = name;
            this.work = work;
            this.age = age;
        }

        public Human(String name, int height, int weight)
        {
            this.name = name;
            this.height = height;
            this.weight = weight;
        }

        public Human(String name, boolean sex, String work)
        {
            this.name = name;
            this.sex = sex;
            this.work = work;
        }
    }
}
