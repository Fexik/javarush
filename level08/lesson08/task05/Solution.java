package com.javarush.test.level08.lesson08.task05;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* Удалить людей, имеющих одинаковые имена
Создать словарь (Map<String, String>) занести в него десять записей по принципу «фамилия» - «имя».
Удалить людей, имеющих одинаковые имена.
*/

public class Solution
{
    public static HashMap<String, String> createMap()
    {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Качур", "Сергей");
        map.put("Швачка", "Ольга");
        map.put("Хома", "Максим");
        map.put("Педро", "Людмила");
        map.put("Педро2", "Александр");
        map.put("Педро3", "Ольга");
        map.put("Табаков", "Александр");
        map.put("Игнатев", "Станислав");
        map.put("Шкаф", "Ольга");
        map.put("Зайцев", "Антон");

        return map;

    }

    public static void removeTheFirstNameDuplicates(HashMap<String, String> map)
    {
        HashMap<String, String> copy1 = new HashMap<String, String>(map);
        HashMap<String, String> copy2 = new HashMap<String, String>(map);

        /*for (Map.Entry<String, String> pair: copy1.entrySet())
        {
            int count = 0;
            for (Map.Entry<String, String> pair2: copy2.entrySet()){
                if (pair.getValue().equals(pair2.getValue())){
                    count++;
                }
            }

            if (count > 1) {
                removeItemFromMapByValue(map, pair.getValue());
            }
        }*/

        for (Map.Entry<String, String> pair: copy1.entrySet())
        {
            int count = 0;
            for (Map.Entry<String, String> pair2: copy1.entrySet()){
                if (pair.getValue().equals(pair2.getValue())){
                    count++;
                }
            }

            if (count > 1) {
                removeItemFromMapByValue(map, pair.getValue());
            }
        }

    }

    public static void removeItemFromMapByValue(HashMap<String, String> map, String value)
    {
        HashMap<String, String> copy = new HashMap<String, String>(map);
        for (Map.Entry<String, String> pair: copy.entrySet())
        {
            if (pair.getValue().equals(value))
                map.remove(pair.getKey());
        }
    }

    public static void main(String[] args)
    {
        HashMap<String, String> map = createMap();
        removeTheFirstNameDuplicates(map);
        System.out.println(map);
    }
}
