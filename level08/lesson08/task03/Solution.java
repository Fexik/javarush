package com.javarush.test.level08.lesson08.task03;

import com.javarush.test.level06.lesson08.task05.StringHelper;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/* Одинаковые имя и фамилия
Создать словарь (Map<String, String>) занести в него десять записей по принципу «Фамилия» - «Имя».
Проверить сколько людей имеют совпадающие с заданным имя или фамилию.
*/

public class Solution
{
    public static HashMap<String, String> createMap()
    {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Хлебов", "Исак");
        map.put("Давидов", "Дима");
        map.put("Зайцев", "Вася");
        map.put("Кнопкин", "Руслан");
        map.put("Васечкин", "Василий");
        map.put("Петечкин", "Игорь");
        map.put("Кастрюлькин", "Иван");
        map.put("Петров", "Александр");
        map.put("Хзкакой", "Петя");
        map.put("Ещехзкакой", "Вася");

        return map;
    }

    public static int getCountTheSameFirstName(HashMap<String, String> map, String name)
    {
        int count = 0;

        for (Map.Entry<String, String> pair : map.entrySet()){
            if (name.equals((String) pair.getValue()))
            {
                count++;
            }
        }
        return count;
    }

    public static int getCountTheSameLastName(HashMap<String, String> map, String familiya)
    {

        int count = 0;
        for (Map.Entry<String, String> pair : map.entrySet())
        {
            if (familiya.equals((String) pair.getKey())){
                count++;
            }
        }
        return count;
    }
}
