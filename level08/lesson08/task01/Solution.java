package com.javarush.test.level08.lesson08.task01;

import java.util.HashSet;
import java.util.Set;

/* 20 слов на букву «Л»
Создать множество строк (Set<String>), занести в него 20 слов на букву «Л».
*/

public class Solution
{
    public static HashSet<String> createSet()
    {
        HashSet<String> list = new HashSet<String>();
        list.add("ламбоджини");
        list.add("ласточка");
        list.add("Людмила");
        list.add("Лаврентий");
        list.add("лук");
        list.add("лимон");
        list.add("лайм");
        list.add("лоадер");
        list.add("лопух");
        list.add("Лорд");
        list.add("лен");
        list.add("Лена");
        list.add("ложка");
        list.add("лапа");
        list.add("лампочка");
        list.add("лопата");
        list.add("ленолиум");
        list.add("люстра");
        list.add("Ляпис");
        list.add("Луна");

        return list;
    }
}
