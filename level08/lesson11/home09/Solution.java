package com.javarush.test.level08.lesson11.home09;

import java.util.Date;

/* Работа с датой
1. Реализовать метод isDateOdd(String date) так, чтобы он возвращал true,
если количество дней с начала года - нечетное число, иначе false
2. String date передается в формате MAY 1 2013
Пример:
JANUARY 1 2000 = true
JANUARY 2 2020 = false
*/

public class Solution
{
    public static void main(String[] args)
    {
        String sDate = "JANUARY 1 2020";
        System.out.println(isDateOdd(sDate));

    }

    public static boolean isDateOdd(String date)
    {
        Date startYear = new Date();
        startYear.setDate(1);
        startYear.setMonth(0);

        Date newDate = new Date(date);

        long diffTime = newDate.getTime() - startYear.getTime();

        //тепер перевести мілісекунди в дні
        long countDays = diffTime / (3600*24*1000);

        return ((countDays % 2) == 0);
    }
}
