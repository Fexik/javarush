package com.javarush.test.level08.lesson11.home06;

/* Вся семья в сборе
1. Создай класс Human с полями имя (String), пол (boolean), возраст (int), дети (ArrayList<Human>).
2. Создай объекты и заполни их так, чтобы получилось: два дедушки, две бабушки, отец, мать, трое детей.
3. Вывести все объекты Human на экран.
*/

import java.util.ArrayList;

public class Solution
{
    public static void main(String[] args)
    {
        ArrayList<Human> children = new ArrayList<Human>();
        Human ch1 = new Human("Олексій", true, 32, new ArrayList<Human>());
        Human ch2 = new Human("Іра", false, 29, new ArrayList<Human>());
        Human ch3 = new Human("Віта", false, 26, new ArrayList<Human>());
        children.add(ch1);
        children.add(ch2);
        children.add(ch3);

        Human father = new Human("Коля", true, 56, children);
        Human mother = new Human("Галя", false, 55, children);

        ArrayList<Human> parrent_f = new ArrayList<Human>();
        ArrayList<Human> parrent_m = new ArrayList<Human>();
        parrent_f.add(father);
        parrent_m.add(mother);
        Human ded1 = new Human("дед Коля", true, 76, parrent_f);
        Human bab1 = new Human("бабушка Наташа", false, 76, parrent_f);

        Human ded2 = new Human("дед Вова", true, 73, parrent_m);
        Human bab2 = new Human("бабушка Люда", false, 78, parrent_m);


        System.out.println(ded1);
        System.out.println(ded2);
        System.out.println(bab1);
        System.out.println(bab2);
        System.out.println(father);
        System.out.println(mother);
        for (Human child:children) System.out.println(child.toString());
    }

    public static class Human
    {
        String name;
        Boolean sex;
        int age;
        ArrayList<Human> children;

        public Human(String name, Boolean sex, int age, ArrayList<Human> children)
        {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.children = children;
        }

        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            int childCount = this.children.size();
            if (childCount > 0)
            {
                text += ", дети: "+this.children.get(0).name;

                for (int i = 1; i < childCount; i++)
                {
                    Human child = this.children.get(i);
                    text += ", "+child.name;
                }
            }

            return text;
        }
    }

}
