package com.javarush.test.level12.lesson04.task03;

/* Пять методов print с разными параметрами
Написать пять методов print с разными параметрами.
*/

public class Solution
{
    public static void main(String[] args)
    {

    }

    //Напишите тут ваши методы
    void print(){}
    void print(int i){}
    void print(String s){}
    void print(String s, int i){}
    void print(int a, int b){}
}
