package com.javarush.test.level22.lesson09.task03;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/* Составить цепочку слов
В методе main считайте с консоли имя файла, который содержит слова, разделенные пробелом.
В методе getLine используя StringBuilder расставить все слова в таком порядке,
чтобы последняя буква данного слова совпадала с первой буквой следующего не учитывая регистр.
Каждое слово должно участвовать 1 раз.
Метод getLine должен возвращать любой вариант.
Слова разделять пробелом.
В файле не обязательно будет много слов.

Пример тела входного файла:
Киев Нью-Йорк Амстердам Вена Мельбурн

Результат:
Амстердам Мельбурн Нью-Йорк Киев Вена
*/
public class Solution {
    public static void main(String[] args) throws IOException
    {
        //...
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Scanner scanner = new Scanner(new FileReader(reader.readLine()));
        String www = "";
        while (scanner.hasNextLine())
        {
            www = scanner.nextLine();
        }
        reader.close();
        scanner.close();
        for (int i = 0; i < 5; i++)
        {

            StringBuilder result = getLine(www.split(" "));
            System.out.println(result.toString());
        }
    }
    public static StringBuilder getLine(String... words) {
        if (words == null || words.length == 0)
            return new StringBuilder();
        if ("".equals(words[0]) || words.length == 1)
            return new StringBuilder(words[0]);

        StringBuilder result = new StringBuilder();

        List<String> list = new ArrayList<>();
        Collections.addAll(list,words);

        while (!isOK(list))
            Collections.shuffle(list);

        for (String s : list)
            result.append(s+ " ");

        result.deleteCharAt(result.length()-1);
        return result;
    }
    public static boolean isOK(List<String> list)
    {
        for (int i = 0; i < list.size()-1; i++)
        {
            String first = list.get(i).toLowerCase();
            String second = list.get(i+1).toLowerCase();

            if (first.charAt(first.length()-1) != second.charAt(0))
                return false;
        }
        return true;
    }
}
