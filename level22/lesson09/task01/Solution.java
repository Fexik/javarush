package com.javarush.test.level22.lesson09.task01;

import java.io.*;
import java.util.*;

/* Обращенные слова
В методе main с консоли считать имя файла, который содержит слова, разделенные пробелами.
Найти в тексте все пары слов, которые являются обращением друг друга. Добавить их в result.
Порядок слов first/second не влияет на тестирование.
Использовать StringBuilder.
Пример содержимого файла
рот тор торт о
о тот тот тот
Вывод:
рот тор
о о
тот тот
*/
public class Solution {
    public static List<Pair> result = new LinkedList<>();

    public static void main(String[] args) {
        List<String> words = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        String path = scanner.nextLine();
        scanner.close();
        try
        {
            BufferedReader fileReader = new BufferedReader(new FileReader(path));

            while (fileReader.ready()){
                words.addAll(Arrays.asList(fileReader.readLine().split(" ")));
            }
            fileReader.close();

            for(int i = 0; i < words.size(); i++)
            {
                for(int j = 0; j < words.size();)
                {
                    if(words.get(j).equals(new StringBuilder(words.get(i)).reverse().toString()) && j != i)
                    {
                        Pair pair = new Pair();
                        pair.first = words.get(j);
                        pair.second = words.get(i);
                        result.add(pair);
                        words.remove(j);
                        words.remove(i);
                        j = 0;
                    }
                    else
                        j++;
                }
            }

        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public static class Pair {
        String first;
        String second;

        @Override
        public String toString() {
            return  first == null && second == null ? "" :
                    first == null && second != null ? second :
                    second == null && first != null ? first :
                    first.compareTo(second) < 0 ? first + " " + second : second + " " + first;

        }
    }

}
