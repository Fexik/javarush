package com.javarush.test.level18.lesson10.bonus01;

/* Шифровка
Придумать механизм шифровки/дешифровки

Программа запускается с одним из следующих наборов параметров:
-e fileName fileOutputName
-d fileName fileOutputName
где
fileName - имя файла, который необходимо зашифровать/расшифровать
fileOutputName - имя файла, куда необходимо записать результат шифрования/дешифрования
-e - ключ указывает, что необходимо зашифровать данные
-d - ключ указывает, что необходимо расшифровать данные
*/

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Solution {
    public static void main(String[] args) throws IOException
    {

        if (args.length < 3) return;
        String fileName = args[1];
        String fileOutputName = args[2];

        //String fileName = "c:/JavaRush/passport3.jpg";
        //String fileOutputName = "c:/JavaRush/passport1.jpg";

        byte secretKey = 45;
        crypt(fileName, fileOutputName, secretKey);

    }



    public static void crypt(String fileName, String fileOutputName, byte secretKey)  throws IOException
    {
        FileInputStream in = new FileInputStream(fileName);
        FileOutputStream out = new FileOutputStream(fileOutputName);

        while (in.available() > 0)
        {
            int b = in.read();
            out.write((b ^ secretKey));
        }

        in.close();
        out.close();
    }
}
