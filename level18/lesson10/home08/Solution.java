package com.javarush.test.level18.lesson10.home08;

import java.io.*;
import java.util.*;

/* Нити и байты
Читайте с консоли имена файлов, пока не будет введено слово "exit"
Передайте имя файла в нить ReadThread
Нить ReadThread должна найти байт, который встречается в файле максимальное число раз,
и добавить его в словарь resultMap,
где параметр String - это имя файла, параметр Integer - это искомый байт.
Закрыть потоки. Не использовать try-with-resources
*/

public class Solution {
    public static Map<String, Integer> resultMap = new HashMap<String, Integer>();

    public static void main(String[] args) throws IOException, InterruptedException
    {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        List<String> fileNames = new ArrayList<String>();
        String str;
        while((str = br.readLine())!= null){
            if (str.equals("exit")) break;
            fileNames.add(str);
        }
        br.close();

        for(String s : fileNames){
            new ReadThread(s).start();
        }


    }

    public static class ReadThread extends Thread {
        private String fileName;
        public ReadThread(String fileName) {
            //implement constructor body
            this.fileName = fileName;
        }
        // implement file reading here - реализуйте чтение из файла тут

        @Override
        public void run()
        {
            List<Integer> list = new ArrayList<>();
            Map<Integer, Integer> map = new HashMap<>();

            try
            {
                FileInputStream in = new FileInputStream(fileName);
                while (in.available()>0){
                    //data = new byte[in.available()];
                    int b = in.read();
                    list.add(b);

                }
                in.close();
            }
            catch (IOException e) {
                //NOP
            }


            for (Integer b : list){
                if (map.containsKey(b)){
                    int count = map.get(b)+1;
                    map.put(b, count);
                }else{
                    map.put(b, 1);
                }
            }
            int maxCount = Integer.MIN_VALUE;
            int maxB = Integer.MIN_VALUE;
            for (Map.Entry<Integer, Integer> entry : map.entrySet()){
                if (entry.getValue() > maxCount){
                    maxCount = entry.getValue();
                    maxB = entry.getKey();
                }
            }

            synchronized (resultMap){
                resultMap.put(this.fileName, maxB);
            }

        }
    }
}
