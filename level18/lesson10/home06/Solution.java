package com.javarush.test.level18.lesson10.home06;

/* Встречаемость символов
Программа запускается с одним параметром - именем файла, который содержит английский текст.
Посчитать частоту встречания каждого символа.
Отсортировать результат по возрастанию кода ASCII (почитать в инете). Пример: ','=44, 's'=115, 't'=116
Вывести на консоль отсортированный результат:
[символ1]  частота1
[символ2]  частота2
Закрыть потоки. Не использовать try-with-resources

Пример вывода:
, 19
- 7
f 361
*/

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

public class Solution {
    public static void main(String[] args) throws IOException{
        String fileName = args[0];
        //String fileName = "c:/JavaRush/data2.txt";

        FileInputStream in = new FileInputStream(fileName);
        Set<Byte> set = new TreeSet<Byte>();
        List<Byte> list = new ArrayList<Byte>();

        while (in.available() > 0){
            byte data = (byte)in.read();
            set.add(data);
            list.add(data);
        }

        byte[] b = new byte[1];
        for (byte symbol : set){
            int count = 0;
            for (byte l : list){
                if (symbol == l) count++;
            }
            b[0] = symbol;
            System.out.println( new String(b) + " " + count);
        }

        in.close();

    }
}
