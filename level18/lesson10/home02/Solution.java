package com.javarush.test.level18.lesson10.home02;

/* Пробелы
В метод main первым параметром приходит имя файла.
Вывести на экран соотношение количества пробелов к количеству всех символов. Например, 10.45
1. Посчитать количество всех символов.
2. Посчитать количество пробелов.
3. Вывести на экран п2/п1*100, округлив до 2 знаков после запятой
4. Закрыть потоки. Не использовать try-with-resources
*/

import java.io.FileInputStream;
import java.io.IOException;

public class Solution {
    public static void main(String[] args) throws IOException{
        String fileName = args[0];
        //String fileName = "c:/JavaRush/data2.txt";
        int countSymbols = 0;
        int countSpace = 0;
        FileInputStream input = new FileInputStream(fileName);

        while (input.available() > 0){
            int data = input.read();
            countSymbols++;
            if (data == Integer.valueOf(' '))
                countSpace++;
        }
        float procent = (float)countSpace/countSymbols*100;

        System.out.printf("%.2f",procent);
        input.close();
    }
}
