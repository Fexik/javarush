package com.javarush.test.level18.lesson10.home04;

/* Объединение файлов
Считать с консоли 2 имени файла
В начало первого файла записать содержимое второго файла так, чтобы получилось объединение файлов
Закрыть потоки. Не использовать try-with-resources
c:/JavaRush/2.txt
c:/JavaRush/data3.txt

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String fileName1 = br.readLine();
        String fileName2 = br.readLine();
        br.close();

        FileInputStream in1 = new FileInputStream(fileName1);
        FileInputStream in2 = new FileInputStream(fileName2);

        byte[] buffer1 = new byte[in1.available()];
        byte[] buffer2 = new byte[in2.available()];
        int count1 = in1.read(buffer1);
        int count2 = in2.read(buffer2);
        in1.close();
        in2.close();

        FileOutputStream out = new FileOutputStream(fileName1);
        byte[] buffer = new byte[count1+count2];
        System.arraycopy(buffer2,0,buffer,0,buffer2.length);
        System.arraycopy(buffer1,0,buffer,buffer2.length,buffer1.length);
        out.write(buffer);
        out.close();

    }
}
