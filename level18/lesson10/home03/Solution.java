package com.javarush.test.level18.lesson10.home03;

/* Два в одном
Считать с консоли 3 имени файла
Записать в первый файл содержимого второго файла, а потом дописать содержимое третьего файла
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String fileName1 = br.readLine();
        String fileName2 = br.readLine();
        String fileName3 = br.readLine();
        br.close();

        FileOutputStream out = new FileOutputStream(fileName1);
        FileInputStream in2 = new FileInputStream(fileName2);
        FileInputStream in3 = new FileInputStream(fileName3);

        while(in2.available() > 0){
            int data = in2.read();
            out.write(data);
        }
        while(in3.available() > 0){
            int data = in3.read();
            out.write(data);
        }
        in2.close();
        in3.close();
        out.close();
    }
}
