package com.javarush.test.level18.lesson10.bonus03;

/* Прайсы 2
CrUD для таблицы внутри файла
Считать с консоли имя файла для операций CrUD
Программа запускается с одним из следующих наборов параметров:
-u id productName price quantity
-d id
Значения параметров:
где id - 8 символов
productName - название товара, 30 chars (60 bytes)
price - цена, 8 символов
quantity - количество, 4 символа
-u  - обновляет данные товара с заданным id
-d  - производит физическое удаление товара с заданным id (все данные, которые относятся к переданному id)

В файле данные хранятся в следующей последовательности (без разделяющих пробелов):
id productName price quantity
Данные дополнены пробелами до их длины

Пример:
19846   Шорты пляжные синие           159.00  12
198478  Шорты пляжные черные с рисунко173.00  17
19847983Куртка для сноубордистов, разм10173.991234
*/

import java.io.*;
import java.util.*;

public class Solution {
    static final int ID_LENGHT = 8;
    static final int PRODUCTNAME_LENGHT = 30;
    static final int PRICE_LENGHT = 8;
    static final int QUANTITY_LENGHT = 4;
    static Map<Integer, String> map = new LinkedHashMap<>();

    public static void main(String[] args) throws Exception {

        BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
        String fileName = console.readLine();
        console.close();

        if (args[0].equals("-c")){
            createItem(fileName, args);
        } else if (args[0].equals("-u")){
            updateItem(fileName, args);
        } else if (args[0].equals("-d")){
            delItem(fileName,args);
        } else {
            return;
        }
    }

    public static Integer getID(String id){
        return Integer.parseInt(id.substring(0,8).replaceAll(" ",""));
    }

    public static String setValidField(String s, int fieldSize){
        int count = fieldSize - s.length();
        if (count != 0)
            for (int i = 0; i < count; i++)
                s += " ";
        return s;
    }

    public static void updateItem(String fileName, String[] args) throws IOException{

        int findID = Integer.parseInt(args[1]);
        String productName = args[2];
        String price = args[3];
        String quantity = args[4];

        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(
                                new File(fileName)),"UTF-8"));
        String s;
        while ((s=in.readLine())!=null){
            map.put(getID(s), s);
        }
        in.close();

        for (Map.Entry<Integer, String> pair : map.entrySet()){
            if (pair.getKey()== findID){
                String row = setValidField(findID+"", ID_LENGHT)
                        + setValidField(productName, PRODUCTNAME_LENGHT)
                        + setValidField(price, PRICE_LENGHT)
                        + setValidField(quantity, QUANTITY_LENGHT)
                        ;
                map.put(findID, row);
                break;
            }
        }

        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(fileName), "UTF-8"));

        for (Map.Entry<Integer, String> pair : map.entrySet()){
            out.append(pair.getValue()+ "\n");
        }
        out.close();
    }

    public static void delItem(String fileName, String[] args) throws IOException{
        int findID = Integer.parseInt(args[1]);
        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(fileName), "UTF-8"));

        for (Map.Entry<Integer, String> pair : map.entrySet()){
            if (pair.getKey()== findID){
                map.remove(findID);
                break;
            }
        }

        for (Map.Entry<Integer, String> pair : map.entrySet()){
            out.append(pair.getValue()+ "\n");
        }
        out.close();
    }

    public static void createItem(String fileName, String[] args) throws IOException{


        String productName = args[1];
        String price = args[2];
        String quantity = args[3];

        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(
                                new File(fileName)),"UTF-8"));
        String s;
        int maxID = 0;

        while ((s=in.readLine())!=null){
            int id = getID(s);
            if (maxID < id ) maxID = id;
        }
        in.close();
        maxID++;

        String row = setValidField(maxID+"", ID_LENGHT)
                + setValidField(productName, PRODUCTNAME_LENGHT)
                + setValidField(price, PRICE_LENGHT)
                + setValidField(quantity, QUANTITY_LENGHT)
                + "\n";

        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(fileName, true), "UTF-8"));
        out.append(row);
        out.close();
    }
}
