package com.javarush.test.level18.lesson03.task02;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/* Минимальный байт
Ввести с консоли имя файла
Найти минимальный байт в файле, вывести его на экран.
Закрыть поток ввода-вывода
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String fileName = br.readLine();
        br.close();
        FileInputStream inputStream = new FileInputStream(fileName);
        int min = inputStream.read();

        while (inputStream.available() > 0){
            int current = inputStream.read();
            if(min > current) min = current;
        }
        inputStream.close();
        System.out.println(min);
    }
}
