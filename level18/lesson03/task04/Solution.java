package com.javarush.test.level18.lesson03.task04;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;

/* Самые редкие байты
Ввести с консоли имя файла
Найти байт или байты с минимальным количеством повторов
Вывести их на экран через пробел
Закрыть поток ввода-вывода
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        // c:/AMD/data.txt
        List<Integer> listBytes = new ArrayList<Integer>();
        Map<Integer,Integer> map = new HashMap<Integer, Integer>();
        int minCount = 1;

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String fileName = br.readLine();
        br.close();
        FileInputStream inputStream = new FileInputStream(fileName);

        while (inputStream.available() > 0){
            int current = inputStream.read();
            listBytes.add(current);
        }
        inputStream.close();

        Set<Integer> setB = new HashSet<Integer>(listBytes);

        for (int b : setB){
            int count = 0;
            for (int listByte : listBytes) {
                if (b==listByte)count++;
            }
            if (count <= minCount)minCount = count;
            map.put(b, count);

        }

        for (Map.Entry<Integer, Integer> pair : map.entrySet()){
            if (pair.getValue().equals(minCount))
                System.out.print(pair.getKey() + " ");
        }
    }
}
