package com.javarush.test.level18.lesson05.task03;

/* Разделение файла
Считать с консоли три имени файла: файл1, файл2, файл3.
Разделить файл1 по следующему критерию:
Первую половину байт записать в файл2, вторую половину байт записать в файл3.
Если в файл1 количество байт нечетное, то файл2 должен содержать бОльшую часть.
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        /*
c:/JavaRush/data.txt
c:/JavaRush/data2.txt
c:/JavaRush/data3.txt
        */
        String fileName1 = br.readLine();
        String fileName2 = br.readLine();
        String fileName3 = br.readLine();
        br.close();
        FileInputStream inputStream = new FileInputStream(fileName1);
        FileOutputStream f2 = new FileOutputStream(fileName2);
        FileOutputStream f3 = new FileOutputStream(fileName3);


        byte[] buffer = new byte[inputStream.available()];
        int count = inputStream.read(buffer);

        int half1 = 0;
        int half2 = 0;
        if ((count % 2) == 0){
            half1 = count / 2;
            half2 = half1;

        }else{
            half1 = (count/2) + (count % 2);
            half2 = count/2;
        }
        f2.write(buffer,0,half1);
        f3.write(buffer,half1,half2);

        inputStream.close();
        f2.close();
        f3.close();
    }
}
