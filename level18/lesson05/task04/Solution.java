package com.javarush.test.level18.lesson05.task04;

/* Реверс файла
Считать с консоли 2 имени файла: файл1, файл2.
Записать в файл2 все байты из файл1, но в обратном порядке
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        /*
c:/JavaRush/data.txt
c:/JavaRush/result.txt

        */
        String fileName1 = br.readLine();
        String fileName2 = br.readLine();
        br.close();
        FileInputStream inputStream = new FileInputStream(fileName1);
        FileOutputStream f2 = new FileOutputStream(fileName2);

        byte[] buffer = new byte[inputStream.available()];

        for (int i = inputStream.read(buffer); i > 0; i--)
        {
            f2.write(buffer[i-1]);
        }

        inputStream.close();
        f2.close();
    }
}
