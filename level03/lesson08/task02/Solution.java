package com.javarush.test.level03.lesson08.task02;

/* Зарплата через 5 лет
Ввести с клавиатуры Имя и два числа, вывести надпись:
name1 получает «число1» через «число2» лет.
Пример: Коля получает 3000 через 5 лет.
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String name = reader.readLine();
        String BabloS = reader.readLine();
        String YearS = reader.readLine();
        int Bablo = Integer.parseInt(BabloS);
        int Year = Integer.parseInt(YearS);

        System.out.println(name + " получает "+ Bablo + " через " + Year + " лет.");


    }
}