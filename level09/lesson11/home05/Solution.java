package com.javarush.test.level09.lesson11.home05;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* Гласные и согласные буквы
Написать программу, которая вводит с клавиатуры строку текста.
Программа должна вывести на экран две строки:
1. первая строка содержит только гласные буквы
2. вторая - только согласные буквы и знаки препинания из введённой строки.
Буквы соединять пробелом.

Пример ввода:
Мама мыла раму.
Пример вывода:
а а ы а а у
М м м л р м .
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<Character> listVowels = new ArrayList<Character>();
        ArrayList<Character> listConsonants = new ArrayList<Character>();

        char[] ch = br.readLine().replaceAll(" ","").toCharArray();
        for (int i = 0; i < ch.length; i++)
        {
            if (isVowel(ch[i])){
                listVowels.add(ch[i]);
            } else {
                listConsonants.add(ch[i]);
            }
        }

        for (char c1 : listVowels)
            System.out.print(c1 + " ");
        System.out.println();
        for (char c2 : listConsonants)
            System.out.print(c2 + " ");



    }


    public static char[] vowels = new char[]{'а', 'я', 'у', 'ю', 'и', 'ы', 'э', 'е', 'о', 'ё'};

    //метод проверяет, гласная ли буква
    public static boolean isVowel(char c)
    {
        c = Character.toLowerCase(c);  //приводим символ в нижний регистр - от заглавных к строчным буквам

        for (char d : vowels)   //ищем среди массива гласных
        {
            if (c == d)
                return true;
        }
        return false;
    }
}
