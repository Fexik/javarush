package com.javarush.test.level20.lesson10.bonus03;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* Кроссворд
1. Дан двумерный массив, который содержит буквы английского алфавита в нижнем регистре.
2. Метод detectAllWords должен найти все слова из words в массиве crossword.
3. Элемент(startX, startY) должен соответствовать первой букве слова, элемент(endX, endY) - последней.
text - это само слово, располагается между начальным и конечным элементами
4. Все слова есть в массиве.
5. Слова могут быть расположены горизонтально, вертикально и по диагонали как в нормальном, так и в обратном порядке.
6. Метод main не участвует в тестировании
*/
public class Solution {
    public static void main(String[] args) {
        int[][] crossword = new int[][]{
                {'f', 'd', 'e', 'r', 'l', 'k'},
                {'u', 's', 'a', 'm', 'e', 'o'},
                {'l', 'n', 'g', 'r', 'o', 'v'},
                {'m', 'l', 'p', 'r', 'r', 'h'},
                {'p', 'o', 'e', 'e', 'j', 'j'}
        };

        List<Word> words = detectAllWords(crossword, "home", "same", "red", "rek", "fsg", "rrj", "gsf", "plg", "vhj", "fulm", "eejj", "rlk", "poe");

        for (Word word : words)
        {
            System.out.println(word);
        }

    }

    public static List<Word> detectAllWords(int[][] crossword, String... words) {

        List<Word> list = new ArrayList<>();
        List<Direction> directions = Direction.getDirections();

        for (String word : words)
        {
            if (word == null || word.equals(" ") || word.equals("")) continue;
            char[] chars = word.toCharArray();
            char firstLetter = chars[0];

                for (int i = 0; i < crossword.length; i++)
                {
                    for (int j = 0; j < crossword[0].length; j++)
                    {
                        if (firstLetter == crossword[i][j])
                        {
                            Word w = new Word(word);
                            for (Direction dir : directions)
                            {
                                try
                                {
                                    if (!hasDirection(crossword, i, j, w, dir)) {
                                        continue;
                                    } else {
                                        list.add(w);
                                        break;
                                    }



                                } catch (ArrayIndexOutOfBoundsException e){
                                    continue;
                                }
                            }

                        }
                    }

                }


        }

        return list;
    }

    public static boolean hasDirection(int[][] crossword, int x, int y, Word word, Direction dir){
        boolean flag = false;
        char[] ch = word.text.toCharArray();
        word.setStartPoint(y, x);
        for (int i = 1; i < ch.length; i++)
        {
            x += dir.x;
            y += dir.y;
            if ((char)crossword[x][y] == ch[i]){
                flag = true;
                word.setEndPoint(y, x);
            } else {
                flag = false;
                break;
            }

        }



        return flag;
    }

    public static class Direction{

        int x;
        int y;

        public Direction(int y, int x)
        {
            this.y = y;
            this.x = x;
        }

        public static List<Direction> getDirections(){
            List<Direction> directions = new ArrayList<>();
            directions.add(new Direction(1, 1));
            directions.add(new Direction(1, 0));
            directions.add(new Direction(1, -1));
            directions.add(new Direction(0, 1));
            directions.add(new Direction(0, -1));
            directions.add(new Direction(-1, 0));
            directions.add(new Direction(-1, 1));
            directions.add(new Direction(-1, -1));
            return directions;
        }
    }

    public static class Word {
        private String text;
        private int startX;
        private int startY;
        private int endX;
        private int endY;

        public Word(String text) {
            this.text = text;
        }

        public void setStartPoint(int i, int j) {
            startX = i;
            startY = j;
        }

        public void setEndPoint(int i, int j) {
            endX = i;
            endY = j;
        }

        @Override
        public String toString() {
            return String.format("%s - (%d, %d) - (%d, %d)", text, startX, startY, endX, endY);
        }
    }
}
