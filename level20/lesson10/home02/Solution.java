package com.javarush.test.level20.lesson10.home02;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

/* Десериализация
На вход подается поток, в который записан сериализованный объект класса A либо класса B.
Десериализуйте объект в методе getOriginalObject предварительно определив, какого именно типа там объект.
Реализуйте интерфейс Serializable где необходимо.
*/
public class Solution implements Serializable{
    public A getOriginalObject(ObjectInputStream objectStream) {


        A a = null;
        try
        {
            Object obj =  objectStream.readObject();
            if (obj instanceof A) {
                a = (A)obj;
            } else if (obj instanceof B){
                a = (B) obj;
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
        return a;
    }

    public class A implements Serializable {
        private static final long serialVersionUID = 1L;
    }

    public class B extends A {
        public B() {
            System.out.println("inside B");
        }
    }
}
