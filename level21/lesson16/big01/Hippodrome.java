package com.javarush.test.level21.lesson16.big01;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fexik on 10.03.2016.
 */
public class Hippodrome
{
    private ArrayList<Horse> horses = new ArrayList<Horse>();

    public static Hippodrome game;

    public static void main(String[] args) throws InterruptedException
    {
        game = new Hippodrome();

        game.getHorses().add(new Horse("Masha", 3, 0));
        game.getHorses().add(new Horse("Nadja", 3, 0));
        game.getHorses().add(new Horse("Zina", 3, 0));

        game.run();

        game.printWinner();
    }

    public ArrayList<Horse> getHorses()
    {
        return horses;
    }

    public void run() throws InterruptedException
    {
        for (int i = 0; i < 100; i++)
        {
            move();
            print();
            Thread.sleep(200);
        }

    }

    public void move(){
        for (Horse horse : horses)
        {
            horse.move();
        }
    }

    public void print(){
        for (Horse horse : horses)
        {
            horse.print();
        }
        System.out.println();
        System.out.println();
        System.out.println();
    }


    public Horse getWinner(){
        double max = 0.0;
        Horse winner = null;
        for (Horse horse : horses)
        {
            if (horse.getDistance() > max){
                max = horse.getDistance();
                winner = horse;
            }
        }

        return winner;
    }

    public void printWinner(){
        System.out.println("Winner is "+ getWinner().getName() +"!");
    }
}
